WORKING_DIR="/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/orthofinder_blast_output/Results_Okt30/WorkingDirectory"
OUTPUT_DIR="$WORKING_DIR"

# Loop over each pair of species files
for query_file in "$WORKING_DIR"/Species*.fa; do
    for subject_file in "$WORKING_DIR"/Species*.fa; do
        # Extract species numbers from filenames to use in output file naming
        query_id=$(basename "$query_file" .fa | sed 's/Species//')
        subject_id=$(basename "$subject_file" .fa | sed 's/Species//')
        
        # Define the output file with OrthoFinder's naming conventions
        output_file="$OUTPUT_DIR/Blast${query_id}_${subject_id}.txt.gz"
        
        # Check if output file already exists, skip if it does
        if [[ -f "$output_file" ]]; then
            echo "File $output_file already exists. Skipping..."
            continue
        fi

        # Run BLASTN with custom parameters and compress the output
        blastn -query "$query_file" -subject "$subject_file" \
               -evalue 1e-3 \
               -qcov_hsp_perc 80 \
               -outfmt 6 \
               -num_threads 25 | gzip > "$output_file"
        
        echo "Generated $output_file"
    done
done
