from Bio import SeqIO
from pathlib import Path
from statistics import median

# Pfade zu Eingabedateien und Ausgabeverzeichnissen
fasta_dir = Path("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/DATA/kmer32_fasta")  # Ordner mit den Eingabedateien
output_dir_median = Path("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/DATA/filtered_fasta/median")
output_dir_longest = Path("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/DATA/filtered_fasta/longest")

# Ausgabeverzeichnisse erstellen, wenn sie nicht existieren
output_dir_median.mkdir(parents=True, exist_ok=True)
output_dir_longest.mkdir(parents=True, exist_ok=True)

# Funktion zum Filtern der Isoformen nach Kriterium (längste oder mediane)
def filter_isoforms(fasta_dir, output_dir, criterion="longest"):
    for fasta_file in fasta_dir.glob("*.fasta"):
        records = list(SeqIO.parse(fasta_file, "fasta"))
        gene_dict = {}

        # Gruppiere Isoformen nach Gen-ID
        for record in records:
            # Nutze die ersten drei Teile der ID (bis zum "g"), um die Gen-ID korrekt zu erfassen
            gene_id_parts = record.id.split('_')
            gene_id = "_".join(gene_id_parts[:3])
            if gene_id not in gene_dict:
                gene_dict[gene_id] = []
            gene_dict[gene_id].append(record)

        filtered_records = []
        for gene_id, isoforms in gene_dict.items():
            if criterion == "longest":
                # Wähle die längste Isoform
                selected_record = max(isoforms, key=lambda x: len(x.seq))
            elif criterion == "median":
                # Wähle die mediane Isoform basierend auf der Länge
                lengths = [len(record.seq) for record in isoforms]
                median_length = median(lengths)
                selected_record = min(isoforms, key=lambda x: abs(len(x.seq) - median_length))

            filtered_records.append(selected_record)

        # Debug-Ausgabe
        print(f"Processing {fasta_file.name}: Found {len(gene_dict)} unique genes, retained {len(filtered_records)} records.")

        # Schreibe gefilterte Isoformen in eine neue FASTA-Datei
        output_file = output_dir / fasta_file.name
        SeqIO.write(filtered_records, output_file, "fasta")
        print(f"Finished processing {fasta_file.name}, saved to {output_file}")

# Filterung anwenden
filter_isoforms(fasta_dir, output_dir_median, criterion="median")
filter_isoforms(fasta_dir, output_dir_longest, criterion="longest")

print("Isoformenfilterung abgeschlossen. Ausgaben für längste und mediane Isoformen gespeichert.")
