import pandas as pd
import subprocess
import os

# Eingabedateien
orthogroups_file = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/filtered/Filtered_Orthogroups.tsv"
species_fasta_dir = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/assembly/"
output_dir = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem"

# Blacklist for species to exclude
blacklist = ["Lasius_fuliginosus"]

# Ensure output directory exists
os.makedirs(output_dir, exist_ok=True)

# Load orthogroups file
print(f"Loading orthogroups file: {orthogroups_file}")
df = pd.read_csv(orthogroups_file, sep="\t")
print("Columns in the DataFrame:", df.columns)

# Iterate through species and process only non-blacklisted ones
for species in df.columns[1:]:  # Skip the first column (Orthogroup ID)
    if species not in df.columns:
        print(f"Species {species} not found in DataFrame columns. Skipping...")
        continue
    if species in blacklist:
        print(f"Skipping blacklisted species: {species}")
        continue

    print(f"Processing {species}...")

    # Path to the species-specific FASTA file
    species_fasta = os.path.join(species_fasta_dir, f"{species}.fasta")
    if not os.path.exists(species_fasta):
        print(f"FASTA file for {species} not found. Skipping...")
        continue

    # Extract transcript IDs for the species
    try:
        species_transcripts = df[["Orthogroup", species]].explode(species).dropna()
    except KeyError as e:
        print(f"Error: {e}. Skipping species {species}.")
        continue

    # Save the mapping file
    mapping_file = os.path.join(output_dir, f"{species}.mapping.txt")
    species_transcripts.to_csv(mapping_file, sep="\t", index=False, header=False)
    print(f"Mapping file written to {mapping_file}")

    # Save the transcript IDs to a list
    transcript_list_file = os.path.join(output_dir, f"{species}_transcripts.txt")
    species_transcripts[species].to_csv(transcript_list_file, index=False, header=False)
    print(f"Transcript list written to {transcript_list_file}")

    # Create species-specific FASTA using SeqKit
    species_output_fasta = os.path.join(output_dir, f"{species}.fasta")
    result = subprocess.run(
        ["seqkit", "grep", "-f", transcript_list_file, species_fasta, "-o", species_output_fasta],
        capture_output=True,
        text=True
    )

    # Handle errors from SeqKit
    if result.returncode != 0:
        print(f"Error processing {species} with SeqKit:")
        print(result.stderr)
    else:
        print(f"Finished processing {species}. Output written to {species_output_fasta}")

print("Preparation complete!")