import pandas as pd
from statistics import median
from Bio import SeqIO
from pathlib import Path
from collections import defaultdict

# Paths to input files and output directories
orthogroups_file = "Orthogroups_processed_at_least_one_gene_per_species_LFUL_Trinity_bidirect_pairwise_length.tsv"
fasta_dir = Path("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/orthofinder_blast_output_filt_bidirect_pairwise_length/Results_Nov10/WorkingDirectory/OrthoFinder/Results_Nov11/Orthogroup_Sequences")
output_dir_median = Path("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/orthofinder_blast_output_filt_bidirect_pairwise_length/Results_Nov10/WorkingDirectory/OrthoFinder/Results_Nov11/Orthogroups/median")
output_dir_longest = Path("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/orthofinder_blast_output_filt_bidirect_pairwise_length/Results_Nov10/WorkingDirectory/OrthoFinder/Results_Nov11/Orthogroups/longest")

# Make output directories if they don't exist
output_dir_median.mkdir(parents=True, exist_ok=True)
output_dir_longest.mkdir(parents=True, exist_ok=True)

# Load Orthogroups.tsv into a DataFrame
orthogroups_df = pd.read_csv(orthogroups_file, sep="\t", index_col=0)

# Function to get isoform lengths from fasta files
def get_isoform_lengths(fasta_dir):
    isoform_lengths = defaultdict(lambda: defaultdict(dict))
    for fasta_file in fasta_dir.glob("*.fa"):
        orthogroup_id = fasta_file.stem
        for record in SeqIO.parse(fasta_file, "fasta"):
            species_id = record.id.split('_')[0]
            gene_id = '_'.join(record.id.split('_')[:2])
            isoform_lengths[orthogroup_id][species_id][record.id] = len(record.seq)
    return isoform_lengths

# Get isoform lengths for each orthogroup
isoform_lengths = get_isoform_lengths(fasta_dir)

# Function to filter by median or longest selection criteria
def filter_orthogroups(orthogroups_df, isoform_lengths, criterion="median"):
    filtered_orthogroups = []
    for orthogroup_id, row in orthogroups_df.iterrows():
        new_row = [orthogroup_id]
        for species_id, genes_str in row.items():
            if pd.isna(genes_str):
                new_row.append("")
                continue
            genes = genes_str.split(", ")
            gene_lengths = {gene: max(isoform_lengths[orthogroup_id][species_id].get(gene, {}).values(), default=0)
                            for gene in genes}
            
            # Select based on median or longest criterion
            if criterion == "median":
                median_length = median(gene_lengths.values())
                selected_gene = min(gene_lengths, key=lambda x: abs(gene_lengths[x] - median_length))
            elif criterion == "longest":
                selected_gene = max(gene_lengths, key=gene_lengths.get)
            
            new_row.append(selected_gene)
        filtered_orthogroups.append(new_row)

    # Create a DataFrame for the filtered orthogroups
    columns = ["Orthogroup"] + orthogroups_df.columns.tolist()
    filtered_df = pd.DataFrame(filtered_orthogroups, columns=columns)
    return filtered_df

# Apply the filtering function for both criteria and save output
median_filtered_df = filter_orthogroups(orthogroups_df, isoform_lengths, criterion="median")
median_filtered_df.to_csv(output_dir_median / "Orthogroups.tsv", sep="\t", index=False)

longest_filtered_df = filter_orthogroups(orthogroups_df, isoform_lengths, criterion="longest")
longest_filtered_df.to_csv(output_dir_longest / "Orthogroups.tsv", sep="\t", index=False)

print("Filtering completed. Median and longest outputs saved.")
