import pandas as pd
import re
import os
import subprocess
from glob import glob

# Define file paths and patterns
input_dir = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/DATA/transdecoder_single_best/pep_file_3"
filtered_dir = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/DATA/transdecoder_single_best/pep_file_3/filtered_fasta"
os.makedirs(filtered_dir, exist_ok=True)

# Get a list of all FASTA files in the input directory
fasta_files = glob(os.path.join(input_dir, "*.fasta"))

# Loop through each FASTA file
for input_filepath in fasta_files:
    filename = os.path.basename(input_filepath)
    headers_to_retain_filepath = os.path.join(filtered_dir, f"{filename}_headers_to_retain.tsv")
    output_filepath = os.path.join(filtered_dir, filename)

    # Step 1: Extract headers using grep command
    headers_filepath = os.path.join(input_dir, f"{filename}_headers.tsv")
    grep_command = f"grep '>' {input_filepath} > {headers_filepath}"
    subprocess.run(grep_command, shell=True, check=True)

    # Step 2: Process headers in Python using pandas
    # Read the extracted headers
    headers = pd.read_csv(headers_filepath, sep="\t", header=None, names=["header"])

    # Extract score and gene information using regex
    headers["score"] = headers["header"].str.extract(r'score=([0-9.-]+)').astype(float)
    headers["gene"] = headers.loc[:, "header"].str.extract(r">(.+)(_i[0-9]+)(.p[0-9]+)?").get(0)

    # Extract the gene base identifier for grouping
    headers["gene_base"] = headers["gene"].str.extract(r"(TRINITY_DN[0-9]+_c[0-9]+_g[0-9]+)").get(0)

    # Group by 'gene_base' and retain only the highest scoring isoform per gene
    # This method ensures the highest score is retained in a robust manner
    highest_scoring = headers.groupby("gene_base", group_keys=False).apply(lambda group: group.loc[group["score"].idxmax()])

    # Debug print to verify deduplication process
    print(f"Deduplicated headers for {filename} - Total unique genes retained: {highest_scoring['gene_base'].nunique()}")

    # Write the full header information to the headers file (only the highest scoring isoform per gene base)
    highest_scoring_headers = highest_scoring["header"].tolist()
    with open(headers_to_retain_filepath, 'w') as f:
        for header in highest_scoring_headers:
            # Remove the ">" symbol at the beginning of each header
            cleaned_header = header.lstrip(">")
            f.write(f"{cleaned_header}\n")

    # Step 3: Run SeqKit command to filter the original FASTA without using regex search
    seqkit_command = f"/storage/mi/arnos98/miniconda3/envs/orthofinder/bin/seqkit grep -n -f {headers_to_retain_filepath} {input_filepath} -o {output_filepath}"
    print(f"Running SeqKit command for {filename}: {seqkit_command}")

    # Execute SeqKit command
    subprocess.run(seqkit_command, shell=True, check=True)

    # Check if the output file is empty
    if os.path.exists(output_filepath) and os.path.getsize(output_filepath) == 0:
        print(f"[ERROR] The output fasta file {output_filepath} is empty for {filename}. Please check SeqKit filtering.")
    else:
        print(f"SeqKit command completed successfully for {filename}. Output written to {output_filepath}.")

print("All FASTA files processed.")
