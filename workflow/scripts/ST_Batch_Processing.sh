#!/bin/bash

# Verzeichnisse definieren
input_dir="/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/DATA/kmer32_fasta"
output_dir="/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/DATA/filtered_fasta/supertranscripts"
trinity_script="/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/thesis_arno_schmidt/workflow/.snakemake/conda/7c64f1c8178146ebac473ae9bd9b60ad_/opt/trinity-2.15.2/Analysis/SuperTranscripts/Trinity_gene_splice_modeler.py"

# Eingabeverzeichnis überprüfen
if [ ! -d "$input_dir" ]; then
    echo "Input directory not found"
    exit 1
fi

# Ausgabeverzeichnis erstellen, falls es noch nicht existiert
mkdir -p "$output_dir"

# Zähler für die Anzahl der bearbeiteten Dateien
file_counter=0
total_files=$(find "$input_dir" -name '*.fasta' | wc -l)

# Schleife durch alle FASTA-Dateien im Eingabeverzeichnis
find "$input_dir" -name '*.fasta' -print0 | while IFS= read -r -d '' fasta_file; do
    # Dateiname extrahieren (ohne Pfad und ohne Erweiterung)
    base_name=$(basename "$fasta_file" .fasta)

    # Output-Prefix definieren
    output_prefix="$output_dir/${base_name}"
    output_fasta="${output_prefix}.fasta"

    # Zähler erhöhen
    ((file_counter++))

    # Prüfen, ob das Supertranskript bereits erstellt wurde
    if [ -f "$output_fasta" ]; then
        echo "[$file_counter/$total_files] Skipping $fasta_file, output already exists."
        continue
    fi

    # Verarbeitungsstatus ausgeben
    echo "[$file_counter/$total_files] Processing $fasta_file..."

    # Befehl ausführen
    python "$trinity_script" \
        --trinity_fasta "$fasta_file" \
        --out_prefix "$output_prefix" || {
        echo "[$file_counter/$total_files] Error processing $fasta_file"
        continue
    }

    # Abschlussmeldung für die aktuelle Datei
    echo "[$file_counter/$total_files] Finished processing $fasta_file"

done

# Abschlussmeldung
echo "All SuperTranscripts have been processed."
