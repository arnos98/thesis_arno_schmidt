import gzip
import os
import shutil
from collections import defaultdict

# Length ratio threshold (e.g., 80% means the shorter length should be at least 80% of the longer one)
LENGTH_RATIO_THRESHOLD = 0.8
WORKING_DIR = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/orthofinder_man_i_30/Results_Okt24/WorkingDirectory"
OUTPUT_DIR = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/orthofinder_blast_output_filt_bidirect_pairwise_length/Results_Nov10/WorkingDirectory"

# Ensure output directory exists
os.makedirs(OUTPUT_DIR, exist_ok=True)

def parse_blast_file(file_path):
    """Parse a BLAST file and return matches as a dictionary."""
    matches = defaultdict(lambda: defaultdict(list))
    with gzip.open(file_path, "rt") as f:
        for line in f:
            fields = line.strip().split("\t")
            if len(fields) < 12:  # Ensure the line has all BLAST output fields
                continue
            query, subject = fields[0], fields[1]
            query_start, query_end, subject_start, subject_end = (
                int(fields[6]), int(fields[7]),
                int(fields[8]), int(fields[9])
            )
            
            # Calculate sequence lengths based on alignment coordinates
            query_len = abs(query_end - query_start) + 1
            subject_len = abs(subject_end - subject_start) + 1
            
            # Check if the shorter sequence is at least 80% the length of the longer one
            shorter_len = min(query_len, subject_len)
            longer_len = max(query_len, subject_len)
            if shorter_len / longer_len >= LENGTH_RATIO_THRESHOLD:
                matches[query][subject] = fields  # Store full line as a list of fields
    return matches

def filter_bidirectional_matches(query_id, subject_id):
    """Filter matches to retain only bidirectional hits that meet the length threshold."""
    file1 = f"Blast{query_id}_{subject_id}.txt.gz"
    file2 = f"Blast{subject_id}_{query_id}.txt.gz"
    file1_path = os.path.join(WORKING_DIR, file1)
    file2_path = os.path.join(WORKING_DIR, file2)
    output_file1_path = os.path.join(OUTPUT_DIR, file1)
    output_file2_path = os.path.join(OUTPUT_DIR, file2)

    # Check if it's a self-alignment file
    is_self_alignment = query_id == subject_id
    if is_self_alignment:
        # Copy self-alignment file directly without bidirectional filtering
        if os.path.exists(file1_path):
            shutil.copyfile(file1_path, output_file1_path)
            print(f"Copied self-alignment file {file1} to output.")
        return

    # Skip processing if either file doesn't exist
    if not os.path.exists(file1_path) or not os.path.exists(file2_path):
        print(f"Skipping pair {query_id}-{subject_id}, one of the files is missing.")
        return

    # Parse both files
    matches1 = parse_blast_file(file1_path)
    matches2 = parse_blast_file(file2_path)

    # Filter for bidirectional matches
    bidirectional_matches1 = []
    bidirectional_matches2 = []
    
    for query in matches1:
        for subject in matches1[query]:
            # Check for reciprocal match in the other file with sufficient length ratio
            if subject in matches2 and query in matches2[subject]:
                bidirectional_matches1.append("\t".join(matches1[query][subject]))
                bidirectional_matches2.append("\t".join(matches2[subject][query]))

    # Save filtered bidirectional matches back to the output directory
    with gzip.open(output_file1_path, "wt") as output_file1:
        for line in bidirectional_matches1:
            output_file1.write(line + "\n")
    with gzip.open(output_file2_path, "wt") as output_file2:
        for line in bidirectional_matches2:
            output_file2.write(line + "\n")

    print(f"Processed pair {query_id}-{subject_id} with {len(bidirectional_matches1)} bidirectional matches.")

# Get all unique pairs of species to process
species_files = [f for f in os.listdir(WORKING_DIR) if f.startswith("Blast") and f.endswith(".txt.gz")]
species_ids = sorted(set(f.split("_")[0].replace("Blast", "") for f in species_files))

# Process each unique pair only once
for i, query_id in enumerate(species_ids):
    for subject_id in species_ids[i:]:
        filter_bidirectional_matches(query_id, subject_id)

print("Pairwise filtering complete. All BLAST files are stored in the new OUTPUT_DIR.")

# Copy non-BLAST files and directories to OUTPUT_DIR to complete the directory structure
for filename in os.listdir(WORKING_DIR):
    source_path = os.path.join(WORKING_DIR, filename)
    destination_path = os.path.join(OUTPUT_DIR, filename)

    if not (filename.startswith("Blast") and filename.endswith(".txt.gz")):
        # Check if the file or directory already exists in the output directory
        if os.path.exists(destination_path):
            print(f"Skipping {filename}, already in the output directory.")
            continue
        # Copy the file or directory
        if os.path.isdir(source_path):
            shutil.copytree(source_path, destination_path)
        else:
            shutil.copy2(source_path, destination_path)

print("Filtering complete. All necessary files are stored in the new OUTPUT_DIR.")
