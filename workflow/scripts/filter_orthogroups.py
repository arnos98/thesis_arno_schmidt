import pandas as pd

def process_and_filter_orthogroups(input_file, intermediate_file, output_exact_file, final_output_file, blacklist=["Lasius_fuliginosus"]):
    # Step 1: Load the Orthogroups.tsv file
    orthogroups_df = pd.read_csv(input_file, sep="\t")

    # Step 2: Remove blacklisted species
    orthogroups_df = orthogroups_df.drop(columns=blacklist, errors="ignore")
    
    # Save intermediate file after removing blacklisted species
    orthogroups_df.to_csv(intermediate_file, sep="\t", index=False)

    # Step 3: Process orthogroups to ensure exactly one unique gene per species
    processed_data = orthogroups_df.copy()
    for index, row in orthogroups_df.iterrows():
        for species in row.index[1:]:  # Skip the first column (Orthogroup ID)
            if pd.notna(row[species]) and isinstance(row[species], str):
                genes = row[species].split(", ")
                # Keep all isoforms but ensure uniqueness by gene base identifier
                unique_genes = {gene.rsplit("_", 1)[0]: gene for gene in genes}
                # Join unique gene identifiers with isoforms back into a string
                processed_data.at[index, species] = ", ".join(unique_genes.values())

    # Filter to retain orthogroups where each species has exactly one unique gene
    exact_gene_data = processed_data[
        processed_data.apply(
            lambda row: all(
                pd.notna(row[species])
                and isinstance(row[species], str)
                and len(row[species].split(", ")) == 1
                for species in row.index[1:]
            ),
            axis=1,
        )
    ]

    # Save the processed data to a file
    exact_gene_data.to_csv(output_exact_file, sep="\t", index=False)

    # Step 4: Filter orthogroups to remove duplicates across groups
    # Flatten the dataframe to check gene occurrences across orthogroups
    gene_occurrences = exact_gene_data.melt(var_name="Orthogroup", value_name="Gene").dropna()

    # Find genes that occur in multiple groups for each species
    duplicate_genes = gene_occurrences[gene_occurrences.duplicated(subset=["Gene"], keep=False)]

    # Get the list of orthogroups containing duplicated genes
    orthogroups_with_duplicates = duplicate_genes["Orthogroup"].unique()

    # Filter out orthogroups with duplicated genes
    filtered_orthogroups_df = exact_gene_data[
        ~exact_gene_data["Orthogroup"].isin(orthogroups_with_duplicates)
    ]

    # Save the final filtered dataframe to a new TSV file
    filtered_orthogroups_df.to_csv(final_output_file, sep="\t", index=False)

# Define file paths
input_file = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/Orthogroups.tsv"
intermediate_file = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/filtered/Orthogroups_30_without_LFUL_Trinity.tsv"
output_exact_file = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/filtered/Orthogroups_30_processed_filtered_single_gene_per_species.tsv"
final_output_file = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/filtered/Filtered_Orthogroups.tsv"

# Run the combined function
process_and_filter_orthogroups(input_file, intermediate_file, output_exact_file, final_output_file)

print(f"Intermediate file with 'LFUL_Trinity' removed saved to {intermediate_file}")
print(f"Processed data with exactly one gene per species saved to {output_exact_file}")
print(f"Final filtered orthogroups saved to {final_output_file}")
