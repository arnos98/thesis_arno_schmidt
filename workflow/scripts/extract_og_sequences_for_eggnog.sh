#!/bin/bash

# Basisverzeichnis für die Eingabedateien
input_dir="/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem"
output_dir="/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/eggNOG/output"
output_fasta="$output_dir/combined_orthogroups.fasta"

# Erstelle das Ausgabeverzeichnis, falls es nicht existiert
mkdir -p "$output_dir"

# Leere die Ausgabe-FASTA-Datei
> "$output_fasta"

# Liste der Spezies (Dateistämme im Eingabeverzeichnis)
species_list=("Camponotus_atriceps" "Camponotus_fallax" "Camponotus_fellah" "Camponotus_floridanus" "Camponotus_maculatus_complex" "Camponotus_nicobarensis" "Camponotus_vicinus")

# Schleife über alle Spezies
for species in "${species_list[@]}"; do
    mapping_file="$input_dir/${species}.mapping.txt"
    fasta_file="$input_dir/${species}.fasta"

    # Prüfen, ob beide Dateien existieren
    if [[ -f $mapping_file && -f $fasta_file ]]; then
        echo "Verarbeite Spezies: $species"

        # Schleife durch jede Zeile der mapping-Datei
        while read -r OG_ID TRANSCRIPT_ID; do
            # Extrahiere die gesamte Sequenz für die aktuelle Transcript ID
            awk -v id="$TRANSCRIPT_ID" -v og="$OG_ID" -v sp="$species" '
            $0 ~ "^>" {header = $0; print_seq = 0}  # Neue Headerzeile erkannt
            $0 ~ "^>" && $0 ~ id {print_seq = 1; print ">" og "|" sp "|" id}  # Passender Header gefunden
            print_seq && $0 !~ "^>" {printf "%s", $0}  # Füge Sequenz zusammen
            END {if (print_seq) print ""}  # Neue Zeile nach vollständiger Sequenz
            ' "$fasta_file" | fold -w 80 >> "$output_fasta"  # Begrenze Sequenz auf 80 Zeichen pro Zeile
        done < "$mapping_file"
    else
        echo "Fehlende Dateien für: $species"
    fi
done

echo "Kombinierte FASTA-Datei erstellt: $output_fasta"
