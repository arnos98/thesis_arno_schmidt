import pandas as pd
import os

sample_dir = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/DATA/subsampled"
output_dir = "../config"

if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Updated species mapping
species_mapping = {
    "CFEL19071": "Camponotus fellah",
    "CFLO19-50": "Camponotus atriceps",
    "CFLO19003": "Camponotus atriceps",
    "CFAL": "Camponotus fallax",
    "CFLO--1--": "Camponotus floridanus",
    "CNIC0": "Camponotus nicobarensis",
    "CMAC19-10": "Camponotus maculatus complex",
    "CMAC19-13": "Camponotus maculatus complex",
    "CMAC119-12": "Camponotus maculatus complex",
    "CVIC": "Camponotus vicinus",
    "LFUL": "Lasius fuliginosus",
}

# Function to extract the base sample name
def extract_base_name(sample_name):
    # Strip the trailing C or V after the last dash (e.g., CFLO--1--C -> CFLO--1--)
    if sample_name.endswith("-C") or sample_name.endswith("-V"):
        return sample_name[:-2]  # Remove the last 2 characters (-C or -V)
    return sample_name

samples = []
for root, dirs, files in os.walk(sample_dir):
    for file in files:
        if file.endswith(".fastq.gz"):
            sample_name = file.split("_")[0]  # Extract the full sample name
            pair = file.split("_")[1].split(".")[0]  # Extract pair (1 or 2)
            
            # Extract the base name and map it to species
            base_sample_name = extract_base_name(sample_name)
            species = species_mapping.get(base_sample_name, "Unknown")
            
            samples.append(
                {
                    "sample": sample_name,  # Use the full sample name with -C or -V
                    "species": species,
                    "file": os.path.join(root, file),
                    "pair": pair,
                }
            )

df = pd.DataFrame(samples)

# Pivot the table to have pairs as columns (1 and 2)
df_pivot = df.pivot(index=["sample", "species"], columns="pair", values="file").reset_index()

# Save to TSV
output_file = os.path.join(output_dir, "samples.tsv")
df_pivot.to_csv(output_file, sep="\t", index=False)
print(f"samples.tsv file generated successfully at {output_file}.")
