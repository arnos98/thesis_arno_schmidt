def get_sample_reads(wildcards):
    samples_df = pd.read_csv("config/samples.tsv", sep='\t')  # Load the TSV dynamically
    reads = samples_df[samples_df['sample'] == wildcards.sample]
    if reads.empty:
        raise ValueError(f"No reads found for sample {wildcards.sample}")
    reads = reads.iloc[0]
    return [reads['1'], reads['2']]

rule fastp_trim:
    input:
        fq1 = lambda wildcards: samples_df[samples_df['sample'] == wildcards.sample]['1'].values[0],
        fq2 = lambda wildcards: samples_df[samples_df['sample'] == wildcards.sample]['2'].values[0]
    output:
        trimmed_fq1 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/trimmed/{species}_{sample}_1.trimmed.fastq.gz",
        trimmed_fq2 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/trimmed/{species}_{sample}_2.trimmed.fastq.gz",
        json_report = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/fastp/{species}_{sample}_fastp.json"
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/fastp/{species}_{sample}.log"
    threads: 50
    conda:
        "../envs/fastp.yaml"
    shell:
        """
        fastp -i {input.fq1} -I {input.fq2} -o {output.trimmed_fq1} \
        -O {output.trimmed_fq2} -j {output.json_report} --thread {threads} > {log} 2>&1
        """

rule kraken_untrimmed:
    input:
        fq1 = lambda wildcards: get_sample_reads(wildcards)[0],
        fq2 = lambda wildcards: get_sample_reads(wildcards)[1]
    output:
        kraken_report = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/kraken/untrimmed/{sample}.kraken.report",
        kraken_output = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/kraken/untrimmed/{sample}.kraken.output"
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/kraken/{sample}.untrimmed.log"
    threads: 50
    conda:
        "../envs/kraken2.yaml"
    shell:
        """
        kraken2 --db {config[kraken2_params][kraken2_db]} --report {output.kraken_report} --paired {input.fq1} {input.fq2} > {output.kraken_output} 2> {log}
        """

rule kraken_trimmed:
    input:
        fq1 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/trimmed/{sample}_1.trimmed.fastq.gz",
        fq2 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/trimmed/{sample}_2.trimmed.fastq.gz"
    output:
        kraken_report = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/kraken/trimmed/{sample}.kraken.report",
        kraken_output = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/kraken/trimmed/{sample}.kraken.output"
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/kraken/{sample}.trimmed.log"
    threads: 50
    conda:
        "../envs/kraken2.yaml"
    shell:
        """
        kraken2 --db {config[kraken2_params][kraken2_db]} --report {output.kraken_report} --paired {input.fq1} {input.fq2} > {output.kraken_output} 2> {log}
        """

rule rna_quast:
    input:
        transcripts = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/assembly/{species}.fasta"
    output:
        outdir = directory("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/rnaquast/{species}"),
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/rnaquast/{species}_rnaquast.log"
    threads: 50
    params:
        busco_lineage = config["busco_params"]["lineage_dir"],
        genemark_path = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/DATA/Genemark"  # Path to GeneMarkS-T
    conda:
        "../envs/rnaquast.yaml"                 
    shell:
        """
        export PATH={params.genemark_path}:$PATH
        rnaQUAST.py \
            -c {input.transcripts} \
            --busco {params.busco_lineage} \
            -t {threads} \
            -o {output.outdir} > {log} 2>&1\
        """

rule qualimap_trinity:
    input:
        bam = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/mapping/{species}/{species}_Trinity_Aligned.sortedByCoord.out.bam"
    output:
        report= directory("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/qualimap/{species}/{species}_trinity_qualimap_report")
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/qualimap/{species}_trinity.log"
    threads: 50
    conda:
        "../envs/qualimap.yaml"
    shell:
        """
        qualimap bamqc -bam {input.bam} -outdir {output.report} --java-mem-size=8G > {log} 2>&1
        """

rule script_prepare_references:
    input:
        orthogroups = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/filtered/Filtered_Orthogroups.tsv",
        trinity_fasta = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/assembly/{species}.fasta"
    output:
        species_fasta = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/{species}.fasta",
        transcript_to_gene = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/{species}.mapping.txt"
    conda:
        "../envs/prepare_references.yaml"
    wildcard_constraints:
        species="|".join(filtered_species)
    script:
        "../scripts/prepare_references.py"


rule rsem_prepare_reference:
    input:
        reference_genome = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/{species}.fasta",
        transcript_to_gene = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/{species}.mapping.txt"
    output:
        seq = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.seq",
        grp = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.grp",
        ti = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.ti",
        transcripts_fa = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.transcripts.fa",
        idx_fa = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.idx.fa",
        n2g_idx_fa = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.n2g.idx.fa",
        bowtie_index_1 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.1.bt2",
        bowtie_index_2 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.2.bt2",
        bowtie_index_3 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.3.bt2",
        bowtie_index_4 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.4.bt2",
        bowtie_rev_1 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.rev.1.bt2",
        bowtie_rev_2 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{species}.rev.2.bt2"
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/rsem/prepare-reference/{species}.log"
    threads: 50
    params:
        extra = lambda wildcards, input: f"--bowtie2 --bowtie2-path /storage/mi/arnos98/miniconda3/envs/bowtie2_env/bin --transcript-to-gene-map {input.transcript_to_gene}"
    wrapper:
        "v5.2.1/bio/rsem/prepare-reference"


# Function to get the species for a given sample
def get_species_for_sample(sample_name):
    # Load the samples.tsv file dynamically
    samples_df = pd.read_csv("config/samples.tsv", sep='\t')
    # Filter the dataframe for the specific sample
    species_row = samples_df[samples_df['sample'] == sample_name]
    # Check if the sample exists
    if species_row.empty:
        raise ValueError(f"Species not found for sample {sample_name}")
    # Return the species name
    return species_row.iloc[0]['species']


rule rsem_calculate_expression:
    input:
        fq_one=lambda wildcards: samples_df[samples_df['sample'] == wildcards.sample]['1'].values[0],
        fq_two=lambda wildcards: samples_df[samples_df['sample'] == wildcards.sample]['2'].values[0],
        reference=lambda wildcards: multiext(f"/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference/{get_species_for_sample(wildcards.sample)}",
                                             ".grp", ".ti", ".transcripts.fa", ".seq", ".idx.fa", ".n2g.idx.fa")
    output:
        genes_results="/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_calculate/{sample}.genes.results",
        isoforms_results="/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_calculate/{sample}.isoforms.results"
    params:
        paired_end=True,
        extra="--seed 42",
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/rsem/calculate_expression/{sample}.log"
    threads: 50
    wrapper:
        "v5.2.1/bio/rsem/calculate-expression"


rule multiqc:
    input:
        kraken_untrimmed_reports = expand("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/kraken/untrimmed/{sample}.kraken.report", sample=samples),
        fastp_reports= expand("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/fastp/{species}_{sample}_fastp.json", species=species, sample=samples),
        kraken_trimmed = expand("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/kraken/trimmed/trimmed_{sample}.kraken.report", sample=samples),
        rnaquast = expand("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/rnaquast/{species}", species=species),
        qualimap = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/qualimap",
        star = expand("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/mapping/{species}/{species}_Trinity_Log.final.out", species=species)
    output:
        html_report  ="/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/multiqc_report.html"
    params:
        data_dir = " ".join([
            "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/kraken/untrimmed",
            "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/fastp",
            "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/kraken/trimmed",
            "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/mapping",
            "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/rnaquast",
            "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/reports/qualimap",
            "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_reference",
            "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/rsem/rsem_calculate"
        ]),
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/multiqc.log"
    conda:
        "../envs/multiqc.yaml"
    shell:
        """
        multiqc {params.data_dir} \
        --outdir {params.data_dir} \
        --filename {output.html_report} > {log} 2>&1
        """
