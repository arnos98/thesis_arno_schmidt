rule merge_reads:
    input:
        fq1_list=lambda wildcards: expand("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/trimmed/{species}_{sample}_1.trimmed.fastq.gz", species=wildcards.species, sample=samples_df[samples_df['species'] == wildcards.species]['sample'].tolist()),
        fq2_list=lambda wildcards: expand("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/trimmed/{species}_{sample}_2.trimmed.fastq.gz", species=wildcards.species, sample=samples_df[samples_df['species'] == wildcards.species]['sample'].tolist())
    output:
        merged_fq1 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/merged/{species}_1.trimmed.fastq.gz",
        merged_fq2 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/merged/{species}_2.trimmed.fastq.gz"
    shell:
        """
        cat {input.fq1_list} > {output.merged_fq1}
        cat {input.fq2_list} > {output.merged_fq2}
        """

rule trinity_assembly:
    input:
        fq1 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/merged/{species}_1.trimmed.fastq.gz",
        fq2 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/merged/{species}_2.trimmed.fastq.gz"
    output:
        fasta = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/assembly/{species}.fasta"
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/assembly/{species}.log"
    params:
        trinity_dir = config["trinity_params"]["outdir"]
    threads: 50
    conda:
        "../envs/trinity.yaml"
    shell:
        """
        # Run Trinity assembly
        Trinity --seqType fq --left {input.fq1} --right {input.fq2} \
        --max_memory 50G --CPU {threads} --output {params.trinity_dir} > {log} 2>&1
        
        # Copy the final Trinity FASTA file with the correct name to the simplified output location
        cp {params.trinity_dir}/trinity_out_dir.Trinity.fasta {output.fasta}
        """

rule star_index_trinity:
    input:
        trinity_fasta = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/assembly/{species}/trinity_out_dir.Trinity.fasta"
    output:
        index_dir = directory("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/mapping/{species}/STAR_index")
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/mapping/{species}_trinity_star_index.log"
    threads: 50
    conda:
        "../envs/star.yaml"
    shell:
        """
        STAR --runMode genomeGenerate --genomeDir {output.index_dir} --genomeFastaFiles {input.trinity_fasta} \
        --runThreadN {threads} --limitGenomeGenerateRAM 90000000000 --genomeSAindexNbases 12 > {log} 2>&1
        """

rule map_reads_trinity:
    input:
        fq1 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/merged/{species}_1.trimmed.fastq.gz",
        fq2 = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/merged/{species}_2.trimmed.fastq.gz",
        trinity_fasta = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/assembly/{species}/trinity_out_dir.Trinity.fasta",
        index_dir = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/mapping/{species}/STAR_index"
    output:
        bam = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/mapping/{species}/{species}_Trinity_Aligned.sortedByCoord.out.bam"
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/mapping/{species}_trinity.log"
    threads: 50
    params:
        out_prefix = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/mapping/{species}/{species}_Trinity_"
    conda:
        "../envs/star.yaml"
    shell:
        """
        STAR --genomeDir {input.index_dir} --readFilesIn {input.fq1} {input.fq2} --readFilesCommand zcat\
        --outFileNamePrefix {params.out_prefix} --outSAMtype BAM SortedByCoordinate > {log} 2>&1
        """

rule orthofinder:
    input:
        trinity_fasta = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/assembly"
    output:
        temp_dir = directory("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/tmp_results")
    log: 
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/orthofinder/orthofinder.log"
    threads: 35
    params:
        inflation = 3.0
    conda:
        "../envs/orthofinder.yaml"
    shell:
        """
        orthofinder -f {input.trinity_fasta} -t {threads} -a {threads} -I {params.inflation} -d -o {output.temp_dir} > {log} 2>&1
        """

rule move_orthofinder_results:
    input:
        temp_dir = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/tmp_results"
    output:
        orthofinder_dir = directory("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results"),
        orthofinder_tsv = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/Orthogroups.tsv"
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/orthofinder/move_results.log"
    shell:
        """
        # Get the latest results directory
        latest_results=$(ls -td {input.temp_dir}/Results_* | head -1)
        if [ -z "$latest_results" ]; then
            echo "Error: No results directory found in {input.temp_dir}" >&2
            exit 1
        fi

        # Ensure the output directory exists
        mkdir -p {output.orthofinder_dir}/Orthogroups

        # Copy only the Orthogroups.tsv file to the output directory
        cp "$latest_results/Orthogroups/Orthogroups.tsv" {output.orthofinder_dir}/Orthogroups

        # Validate that the expected output file exists
        if [ ! -f {output.orthofinder_dir}/Orthogroups/Orthogroups.tsv ]; then
            echo "Error: Expected output file {output.orthofinder_dir}/Orthogroups/Orthogroups.tsv not found!" >&2
            exit 1
        fi
        """

rule filter_orthogroups:
    input:
        orthofinder_tsv = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/Orthogroups.tsv"
    output:
        intermediate_file = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/filtered/Orthogroups_30_without_LFUL_Trinity.tsv",
        output_exact_file = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/filtered/Orthogroups_30_processed_filtered_single_gene_per_species.tsv",
        final_output_file = "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/filtered/Filtered_Orthogroups.tsv",
        filtered_dir = directory("/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/orthofinder/results/Orthogroups/filtered")
    log:
        "/buffer/ag_bsc/Theses/RNA_Arno_Schmidt/output/logs/orthofinder_processed/processing.log"
    conda:
        "../envs/orthogroup_processing.yaml"
    shell:
        """
        mkdir -p $(dirname {output.filtered_dir})
        python3 /buffer/ag_bsc/Theses/RNA_Arno_Schmidt/thesis_arno_schmidt/workflow/scripts/filter_orthogroups.py
        """